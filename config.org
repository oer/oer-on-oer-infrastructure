#+LANGUAGE: en
#+INCLUDE: "emacs-reveal/org/config.org"

# Show section numbers up to 3rd-level
#+OPTIONS: num:3

#+REVEAL_THEME: dbis

# Use different title slide.
#+REVEAL_TITLE_SLIDE: emacs-reveal/title-slide/ds.html

# Legalese in preamble
#+REVEAL_PREAMBLE: <div class="legalese"><p><a href="/imprint.html">Imprint</a> | <a href="/privacy.html">Privacy Policy</a></p></div>
